import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Loan } from './loan.model';
import { LoanService } from './loan.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { LoginProfileService } from '../login/login-profile.service';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
})
export class LoanComponent implements OnInit {
  @ViewChild('loanTypeInput', {static: false}) loanTypeInputRef: ElementRef;
  @ViewChild('loanAmountInput', {static: false}) loanAmountInputRef: ElementRef; 
  @ViewChild('rateOfInterestInput', {static: false}) rateOfInterestInputRef: ElementRef;
  @ViewChild('durationOfLoanInput', {static: false}) durationOfLoanInputRef: ElementRef;
  loan: Loan;
  loanType: string = 'Select Loan type';
  loanAmount: number;  
  rateOfInterest: number;
  durationOfLoan: number;
  loanIssueDateModel: NgbDateStruct;
  loanApplyDateModel: NgbDateStruct;
  date: {year: number, month: number};
  
  constructor(private loanService: LoanService,private loginService: LoginProfileService) { }

  ngOnInit(): void {
  }

  applyLoan() {
    this.loan = new Loan();
    this.loan.loanUser=this.loginService.getUserName();
    this.loan.loanType = this.loanType;
    this.loan.loanAmount = this.loanAmountInputRef.nativeElement.value;
    this.loan.loanApplyDate = 
                  this.loanApplyDateModel.year+"/"+
                  this.loanApplyDateModel.month+"/"+
                  this.loanApplyDateModel.day;
    this.loan.loanIssueDate = 
                  this.loanIssueDateModel.year+"/"+
                  this.loanIssueDateModel.month+"/"+
                  this.loanIssueDateModel.day; 
    this.loan.rateOfInterest = this.rateOfInterest;
    this.loan.durationOfLoan = this.durationOfLoan;
    
    this.loanService.createLoan(this.loan);
  } 

  onSelectDropdown(value: string) {
    console.log ('selected value -'+value);
    this.loanType = value;
    if (this.loanType === 'Education Loan') {
      this.rateOfInterest = 5.2;
      console.log('ROI '+this.rateOfInterest);
    } else if (this.loanType == 'Home Loan') {
      this.rateOfInterest = 7.25;
    } else {
      this.rateOfInterest = 8;
    }
  }

  onSelectRateDropdown(value:number) {
    console.log('Rate of interest selected '+value);
    this.durationOfLoan = value;
  }

}
