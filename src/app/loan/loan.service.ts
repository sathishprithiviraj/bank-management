import { Loan } from "./loan.model";
import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class LoanService {
    private loan: Loan[] = [];
    createLoan(loan:Loan) {
        console.log("loan - "+loan);
        console.log("loan type - "+ loan.loanType);
        this.loan.push(loan);
    }

    getLoans() {
        return this.loan;
    }
}