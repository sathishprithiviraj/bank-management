export class Loan {
    loanUser: string;
    loanType: string;
    loanAmount: number;
    loanApplyDate: string;
    loanIssueDate: string;
    rateOfInterest: number;
    durationOfLoan: number;
    annualIncome: number;
    companyName: string;
    designation: string;
    totalExp: number;
    expWithCurrComp: number;
    coureFee: number;
    course: string;
    fathersName: string;
    fathersOccupation: string;
    fathersTotalExp: number;
    fathersExpWithCurrComp: number;
    rationCardNo: string;
    
    // constructor(loanType: string, 
    //     loanAmount:number, 
    //     loanApplyDate: Date, 
    //     loanIssueDate:Date, 
    //     rateOfInterest: number, 
    //     durationOfLoan: number)    {
    //         this.loanType = loanType;
    //         this.loanAmount = loanAmount;
    //         this.loanApplyDate = loanApplyDate;
    //         this.loanIssueDate = loanIssueDate;
    //         this.rateOfInterest = rateOfInterest;
    //         this.durationOfLoan = durationOfLoan;
    //     }
}