import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanComponent } from './loan.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoanService } from './loan.service';
import { LoginProfileService } from '../login/login-profile.service';
import { Loan } from './loan.model';

describe('LoanComponent', () => {
  let component: LoanComponent;
  let fixture: ComponentFixture<LoanComponent>;
  let loanService: LoanService;
  let loginService: LoginProfileService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, NgbModule],
      declarations: [ LoanComponent ],
      providers: [LoanService, LoginProfileService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanComponent);
    component = fixture.componentInstance;
    loanService = fixture.debugElement.injector.get(LoanService);
    loginService = fixture.debugElement.injector.get(LoginProfileService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it ('Apply loan', ()=> {
    let loan = new Loan();
    loan.loanUser = "testuser";
    loan.loanType="Education";
    loan.loanAmount = 10000;
    component.loanIssueDateModel={year:2021,month:10,day:1};
    component.loanApplyDateModel={year:2021,month:10,day:1};
    loanService.createLoan(loan);
    let loans: Loan[] = loanService.getLoans();
    expect(loans[0].loanUser).toEqual('testuser');
    loginService.setUserName(loan.loanUser);
    component.applyLoan();
    expect(component.loan.loanUser).toEqual(loan.loanUser);
    spyOn(loanService, 'getLoans').and.returnValue(loans);
    let loanres = loanService.getLoans();
    expect(loanres[0].loanAmount).toEqual(10000);
  });

});
