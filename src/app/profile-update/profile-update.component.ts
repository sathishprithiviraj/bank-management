import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CustomerService } from '../registration/customer.service';
import { Customer } from '../registration/customer.model';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-profile-update',
  templateUrl: './profile-update.component.html',
  styleUrls: ['./profile-update.component.css']
})
export class ProfileUpdateComponent implements OnInit {
  error: boolean;
  errorMsg: string;
  updateCustomerObj:Customer;  
  dobModel: NgbDateStruct;
  date: {year: number, month: number};
  registrationDateModel: NgbDateStruct;
  
  constructor(private customerService: CustomerService, private route: ActivatedRoute,
    private routeNav: Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params:ParamMap)=> {
      let userName = params.get('username');      
      this.updateCustomerObj = this.customerService.searchCustomerByUserName(userName);
      let c_dob = new Date(this.updateCustomerObj.dob);
      this.dobModel= {year:c_dob.getFullYear(), month: c_dob.getMonth()+1, day: c_dob.getDate()};
      let c_regDate = new Date(this.updateCustomerObj.registrationDate);
      this.registrationDateModel = {year:c_regDate.getFullYear(), month: c_regDate.getMonth()+1, day: c_regDate.getDate()};
   //   this.registrationDateModel.year= c_regDate.getFullYear();
    //  this.registrationDateModel.month = c_regDate.getMonth();
    //  this.registrationDateModel.day = c_regDate.getDay();
      //formatDate(this.updateCustomerObj.dob, 'YYYY/MM/DD', 'IST');
      
      console.log('This customer '+this.updateCustomerObj.customerId);
    })
  }

  
  updateProfile() {  
      
    this.updateCustomerObj.dob=this.dobModel.year+"/"+this.dobModel.month+"/"+this.dobModel.day;
    this.updateCustomerObj.registrationDate=this.registrationDateModel.year+"/"
                  +this.registrationDateModel.month
                  +"/"+this.registrationDateModel.day;
    this.customerService.updateCustomer(this.updateCustomerObj);
    this.routeNav.navigate(['/home']);
  }
}
