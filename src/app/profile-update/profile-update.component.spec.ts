import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileUpdateComponent } from './profile-update.component';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { CustomerService } from '../registration/customer.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Customer } from '../registration/customer.model';

describe('ProfileUpdateComponent', () => {
  let component: ProfileUpdateComponent;
  let fixture: ComponentFixture<ProfileUpdateComponent>;
  let customerService: CustomerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, NgbModule, RouterTestingModule],
      declarations: [ ProfileUpdateComponent ],
      providers: [CustomerService]
    })
    .compileComponents();
  }));
   it('should create', () => {
    fixture = TestBed.createComponent(ProfileUpdateComponent);
    component = fixture.componentInstance;  
     expect(component).toBeTruthy();
   });

   it('update profile', () => {
     fixture = TestBed.createComponent(ProfileUpdateComponent);
     component = fixture.componentInstance;  
     customerService = fixture.debugElement.injector.get(CustomerService);
     let customer: Customer = new Customer();
     customer.name = 'sathish';
     customer.username = 'sathish';
     customer.dob = "2012/1/10";
     component.dobModel = {year:2012, month: 10, day: 30};
     component.registrationDateModel = {year:2012, month: 10, day: 30};
     component.updateCustomerObj = customer;
     component.updateProfile();      
     spyOn(component, 'updateProfile');
     spyOn(customerService, 'searchCustomer').and.returnValue(customer);
     spyOn(customerService, 'updateCustomer');
     let result =customerService.searchCustomer(customer);     
     expect(customerService.updateCustomer(customer)).toHaveBeenCalledBefore;
     expect(component.updateProfile()).toHaveBeenCalledBefore;
     expect(result.username).toEqual(customer.username);
   });

   
});
