import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { LoanComponent } from './loan/loan.component';
import { ProfileUpdateComponent } from './profile-update/profile-update.component';
import { CustomerService } from './registration/customer.service';
import { LoanService } from './loan/loan.service';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { LoginProfileService } from './login/login-profile.service';
import { EductionalComponent } from './loan/eductional/eductional.component';
import { PersonalHomeComponent } from './loan/personal-home/personal-home.component';
import { LoanViewComponent } from './home/loan-view/loan-view.component';
import { AuthGuard } from './login/auth-guard.service';
import { AuthService } from './login/auth.service';
const appRoutes: Routes = [
  {path: "", component:LoginComponent },
  {path: "home", canActivate: [AuthGuard], component:HomeComponent },
  {path: "applyloan", canActivate: [AuthGuard], component: LoanComponent},
  {path: "registration", component: RegistrationComponent},
  {path: "user-profile/:username", canActivate: [AuthGuard], component:ProfileUpdateComponent }
] 
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    RegistrationComponent,
    ProfileUpdateComponent,
    LoanComponent,
    EductionalComponent,
    PersonalHomeComponent,
    LoanViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [CustomerService,LoanService, LoginProfileService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
