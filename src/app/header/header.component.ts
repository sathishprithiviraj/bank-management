import { Component, OnInit } from '@angular/core';
import { LoginProfileService } from '../login/login-profile.service';
import { AuthService } from '../login/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userName: string;
  loggedInStatus: boolean;
  constructor(private loginService: LoginProfileService, 
              private authService: AuthService,
              private router: Router) { 
      this.authService.logInStatus.subscribe((status:boolean)=>{
        setTimeout(()=> {
          this.loggedInStatus = status;
        }, 800)        
      });
  }
  
  ngOnInit(): void {
    console.log ("HEADER"+this.loginService.getUserName());
    this.userName = this.loginService.getUserName();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

}
