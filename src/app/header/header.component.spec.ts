import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { AuthService } from '../login/auth.service';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginProfileService } from '../login/login-profile.service';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let authService: AuthService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        LoginProfileService, AuthService, RouterTestingModule
      ],
      declarations: [ HeaderComponent],
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    authService = fixture.debugElement.injector.get(AuthService);
    fixture.detectChanges();
  });

  it('create component ', () => {
    expect(component).toBeTruthy();
  });
 
  it ('User logout test',() => {
    authService.logout();
    expect(authService.loggedIn).toEqual(false);
  });
});
