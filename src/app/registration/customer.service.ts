import { Customer } from "./customer.model";
import { OnInit, Injectable } from "@angular/core";
import { formatDate } from "@angular/common";

@Injectable({ providedIn: 'root' })
export class CustomerService {
    customers: Customer[] = [];
    customerId:string;
    accountNumber: number = 1111111111111111;

    initialize() {
        let defaultCustomer: Customer= new Customer();
        defaultCustomer.customerId="R-101";
        defaultCustomer.name="P.C.Sathish";
        defaultCustomer.username="sathish";
        defaultCustomer.password="test";
        defaultCustomer.guardianType="nil";
        defaultCustomer.guardianName="nil";
        defaultCustomer.address="test address";
        defaultCustomer.citizenship="Indian";
        defaultCustomer.state="Tamilnadu";
        defaultCustomer.country="India";
        defaultCustomer.email="test@test.com";
        defaultCustomer.gender="male";
        defaultCustomer.maritalStatus="Married";
        defaultCustomer.contact="1234567890";
        defaultCustomer.dob = "2000/10/30";
        defaultCustomer.registrationDate="2021/10/30";
        defaultCustomer.bankName="Test Bank";
        defaultCustomer.accountType="Saving";
        defaultCustomer.branchName="Thiruvanmiyur branch";
        defaultCustomer.citizenShipStatus="Normal";
        defaultCustomer.initialDepositAmount=10000;
        defaultCustomer.idProofType="PAN Card";
        defaultCustomer.idDocNumber=1223444;
        defaultCustomer.refHolderName="nil";
        defaultCustomer.refAccHolderNo=0;
        defaultCustomer.refAccHolderAddress="nil";
        defaultCustomer.accountNumber=4444444444444444;
        this.customers.push(defaultCustomer);
        console.log("customes array- "+this.customers);
    }

   
    addCustomer(customer:Customer) {
        let userNameExists = false;
        for (let c of this.customers) { 
            if (c.username === customer.username) {
                console.log("Username "+c.username);
                userNameExists = true;
                break;
            }
        }
        if (!userNameExists) {
            this.customers.push(customer);
            console.log("Added Customer "+customer.username);
            return true;
        } else {
            console.log("User name exists. Please select a different username");
            return false;
        }
        
    }
    updateCustomer(customer:Customer) {
        for (let c of this.customers) { 
            if (c.username == customer.username) {
                c = customer;
            }
        }      
    }

    searchCustomer(sc:Customer): Customer {
        for (let customer of this.customers) {
            console.log("Customer - "+customer.username+", "+customer.password);
            if (sc.username === customer.username && sc.password===customer.password) {
                return customer;
            }
        }
        return undefined;
    }

    searchCustomerByUserName(userName:string): Customer {
        for (let customer of this.customers) {
            console.log("Customer - "+customer);
            if (userName === customer.username) {
                return customer;
            }
        }
        return undefined;
    }

    getCustomerId() {        
        return "R-"+Math.trunc(Math.random()*1000);
    }

    getAccountNumber() {
        return this.accountNumber++;
    }
}