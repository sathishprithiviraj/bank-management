import { formatDate } from "@angular/common";

export class Customer {
    customerId: string;
    name: string;
    accountNumber: number;
    username: string;
    password: string;
    guardianType: string;
    guardianName: string;
    address: string;
    citizenship: string;
    state: string;
    country: string;
    email: string;
    gender: string;
    maritalStatus: string;
    contact: string;
    dob: string;
    registrationDate = formatDate(Date.now(), "yyyy/mm/dd",'en_US');
    bankName: string;    
    accountType: string='Select account type';
    branchName: string;
    citizenShipStatus: string='Choose status';
    initialDepositAmount: number=0;
    idProofType: string;
    idDocNumber: number;
    refHolderName: string;
    refAccHolderNo: number;
    refAccHolderAddress: string;
    constructor() {}
}