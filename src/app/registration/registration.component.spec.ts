import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationComponent } from './registration.component';
import { FormsModule } from '@angular/forms';
import { NgbModule, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { CustomerService } from './customer.service';
import { Customer } from './customer.model';

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;
  let customerService: CustomerService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, NgbModule],
      providers: [CustomerService],
      declarations: [ RegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    customerService= fixture.debugElement.injector.get(CustomerService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Register customer', () => {
    let customer = new Customer();
    let testDobModel: NgbDateStruct;
    customer.name="test";
    customer.username="testuser";
    customer.password="test";
    customer.gender = "male";
    customer.country="india";
    customer.initialDepositAmount=5000;
    customer.accountType="saving";
    let cdobModel = {year: 2012, month:1, day:1};
    component.dobModel = cdobModel;
    customerService.addCustomer(customer);
    component.onRegister();    
    expect(component.error).toBe(false);
    expect(component.registered).toEqual(true);
    component.onSelectAccountDropdown('saving');
    expect(component.customer.accountType).toEqual('saving');
    component.onSelectCitizenStatusDropdown('Indian');
    expect(component.customer.citizenShipStatus).toEqual('Indian');
  });

  it('Customer already exists', () => {
    let cdobModel = {year: 2012, month:1, day:1};
    component.dobModel = cdobModel;

    let customer = new Customer();
    let testDobModel: NgbDateStruct;
    customer.name="test";
    customer.username="testuser";
    customer.password="test";
    customer.gender = "male";
    customer.country="india";
    customer.initialDepositAmount=5000;
    customer.accountType="saving";

    let customer1 = new Customer();
    customer1.name="test";
    customer1.username="testuser";
    customer1.password="test";
    customer1.gender = "male";
    customer1.country="india";
    customer1.initialDepositAmount=5000;
    customer1.accountType="saving";
  
    customerService.addCustomer(customer);
    component.onRegister();  
    customerService.addCustomer(customer1);
    component.onRegister();  
    expect(component.errorMsg).toEqual(
      'User Name already exists. Please select a different username');
  });
});
