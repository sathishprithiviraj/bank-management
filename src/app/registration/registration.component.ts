import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Customer } from './customer.model';
import { CustomerService } from './customer.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
   
  // @ViewChild('name', {static: false}) nameRef: ElementRef;
  // @ViewChild('usernameRef', {static: false}) usernameRef: ElementRef;
  // @ViewChild('passwordRef', {static: false}) passwordRef: ElementRef;
  // @ViewChild('guardiantypeRef', {static: false}) guardiantypeRef: ElementRef;
  // @ViewChild('guardianNameRef', {static: false}) guardianNameRef: ElementRef;
  // @ViewChild('addressRef', {static: false}) addressRef: ElementRef;
  // @ViewChild('citizenshipRef', {static: false}) citizenshipRef: ElementRef;
  // @ViewChild('stateRef', {static: false}) stateRef: ElementRef;
  // @ViewChild('countryRef', {static: false}) countryRef: ElementRef;
  // @ViewChild('emailRef', {static: false}) emailRef: ElementRef;
  // @ViewChild('genderRef', {static: false}) genderRef: ElementRef;
  // @ViewChild('maritalRef', {static: false}) maritalRef: ElementRef;
  // @ViewChild('contactRef', {static: false}) contactRef: ElementRef;
  // @ViewChild('accounttypeRef', {static: false}) accounttypeRef: ElementRef;
  // @ViewChild('branchNameRef', {static: false}) branchNameRef: ElementRef;
  // @ViewChild('citizenStatusRef', {static: false}) citizenStatusRef: ElementRef;
  // @ViewChild('initialDepositAmtRef', {static: false}) initialDepositAmtRef: ElementRef;
  // @ViewChild('idProofTypeRef', {static: false}) idProofTypeRef: ElementRef;
  // @ViewChild('idDocumentNoRef', {static: false}) idDocumentNoRef: ElementRef;
  // @ViewChild('refAccHolderName', {static: false}) refAccHolderName: ElementRef;
  // @ViewChild('refAccHolderNo', {static: false}) refAccHolderNo: ElementRef;
  // @ViewChild('refAccHolderAddress', {static: false}) refAccHolderAddress: ElementRef;
  errorMsg: string;
  error: boolean;
  dobModel: NgbDateStruct;
  registrationDateModel: NgbDateStruct;
  date: {year: number, month: number};
  customer: Customer = new Customer();
  registered: boolean=false;
  constructor(private customerService: CustomerService) { }
  newCustomer: Customer;
  ngOnInit(): void {    
    
    

  }

  onRegister() {
    
     this.newCustomer = new Customer();
     this.newCustomer.customerId = this.customerService.getCustomerId();
     this.newCustomer.accountNumber = this.customerService.getAccountNumber();
     this.newCustomer.name = this.customer.name;
     this.newCustomer.username = this.customer.username;
     this.newCustomer.password = this.customer.password;
     this.newCustomer.guardianType = this.customer.guardianType;
     this.newCustomer.guardianName = this.customer.guardianName;
     this.newCustomer.address = this.customer.address;
     this.newCustomer.citizenship = this.customer.citizenship;
     this.newCustomer.citizenShipStatus = this.customer.citizenShipStatus;
     this.newCustomer.state = this.customer.state;
     this.newCustomer.country = this.customer.country;
     this.newCustomer.email = this.customer.email;
     this.newCustomer.gender = this.customer.gender;
     this.newCustomer.maritalStatus = this.customer.maritalStatus;
     this.newCustomer.contact = this.customer.contact;
     this.newCustomer.accountType = this.customer.accountType;
     this.newCustomer.branchName =this.customer.branchName;
     this.newCustomer.citizenShipStatus = this.customer.citizenShipStatus;
     this.newCustomer.initialDepositAmount = this.customer.initialDepositAmount;
     this.newCustomer.idProofType = this.customer.idProofType;
     this.newCustomer.idDocNumber = this.customer.idDocNumber;
     this.newCustomer.refHolderName = this.customer.refHolderName;
     this.newCustomer.refAccHolderNo = this.customer.refAccHolderNo;
     this.newCustomer.refAccHolderAddress = this.customer.refAccHolderAddress;
    // this.customer.name = this.nameRef.nativeElement.value;
    // this.customer.username = this.usernameRef.nativeElement.value;
    // this.customer.password = this.passwordRef.nativeElement.value;
    // this.customer.guardianType = this.guardiantypeRef.nativeElement.value;
    // this.customer.guardianName = this.guardianNameRef.nativeElement.value;
    // this.customer.address = this.addressRef.nativeElement.value;
    if (this.newCustomer.initialDepositAmount<5000 &&
      this.newCustomer.accountType == 'Saving')  {
      this.error = true;
      this.errorMsg = "Savings account cannot have initial deposit amount less than 5000";
      return;
    }
    let c_dob = new Date(this.dobModel.year, this.dobModel.month, this.dobModel.day);
    let months =  c_dob.getTime() - Date.now();
    console.log ("Age in months- "+months)
    console.log("adding customer "+this.newCustomer);
    console.log("Date selected "+JSON.stringify(this.dobModel));
    console.log("Reg Date selected "+JSON.stringify(this.registrationDateModel));
    let success = this.customerService.addCustomer(this.newCustomer);
    if (!success) {
      this.errorMsg = "User Name already exists. Please select a different username";
      this.error = true;
      this.registered = false;
    } else {
      this.errorMsg = "";
      this.error = false;
      this.registered = true;
    }
    
  }

  onSelectAccountDropdown(value: string) {
    this.customer.accountType = value;
  }

  onSelectCitizenStatusDropdown(value:string) {
    this.customer.citizenShipStatus = value;
  }

}
