import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class LoginProfileService {
    private name: string;
    private userName: string;
    private authenticated: boolean;
   
    getUserName() {
        return this.userName;
    }
    getName() {
        return this.name;
    }
    setUserName(userName: string) { 
        this.userName = userName;
    }
    setName(name: string) {
        this.name = name;
    }
    setAuthenticated(auth:boolean) {
        this.authenticated = auth;
    }

    isAuthenticated() {
        return this.authenticated;
    }
}