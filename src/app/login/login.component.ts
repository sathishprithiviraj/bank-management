import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CustomerService } from '../registration/customer.service';
import { Customer } from '../registration/customer.model';
import { Router } from '@angular/router';
import { LoginProfileService } from './login-profile.service';
import { AuthService } from './auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // @ViewChild('userNameInput', {static: false}) userNameInputRef: ElementRef;
  // @ViewChild('passwordInput', {static: false}) passwordInputRef: ElementRef;
  errorMsg: string;
  userName: string;
  password: string;
  

  constructor(private customerService: CustomerService, 
    private loginProfileService:LoginProfileService, 
    private routeNav: Router,
    private authService:AuthService) { }

  ngOnInit(): void {
    console.log("initializing ....");
    this.customerService.initialize();
  }
  login() {
   
    const login:Customer  = new Customer();
    login.username=this.userName;
    login.password=this.password;
    const result:Customer = this.customerService.searchCustomer(login);
    if (result) {
      console.log('Login success');
      this.authService.login();
      
      this.loginProfileService.setUserName(result.username);
      this.loginProfileService.setName(result.name);
      this.loginProfileService.setAuthenticated(true);
      this.routeNav.navigate(['/home']);
    } else {
      this.loginProfileService.setAuthenticated(false);
      console.log('Login Failure');
      this.errorMsg = "Login failed";
    }
  }

  goToRegistration() {
    this.routeNav.navigate(['/registration']);
  }
}
