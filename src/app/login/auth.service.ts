import { EventEmitter } from "@angular/core";

export class AuthService {
    loggedIn = false;
    logInStatus = new EventEmitter<boolean>();
    isAuthenticated() {
        const promise = new Promise(
            (resolve, reject) => {
                setTimeout(()=> {
                    resolve(this.loggedIn);
                }, 800);
            }
        );
        return promise;
    }
    login() {
        this.loggedIn = true;
        this.logInStatus.emit(true);
    }

    logout() {
        this.loggedIn = false;
        this.logInStatus.emit(false);
    }
}