import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import { LoginComponent } from './login.component';
import { CustomerService } from '../registration/customer.service';
import { LoginProfileService } from './login-profile.service';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Customer } from '../registration/customer.model';
import { AuthService } from './auth.service';

describe('LoginComponent testing', () => {
  let component: LoginComponent;
  let loginService: LoginProfileService;
  let customerService: CustomerService;
  let authService: AuthService;
  
  let fixture: ComponentFixture<LoginComponent>;
  let router: Router;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule],
      providers: [FormsModule, AuthService, CustomerService, LoginProfileService],
      declarations: [ LoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    customerService= fixture.debugElement.injector.get(CustomerService);
    authService = fixture.debugElement.injector.get(AuthService);
    loginService = fixture.debugElement.injector.get(LoginProfileService);
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should login', () => {    
    let customer = new Customer();
    customer.username = 'testuser';
    customer.password = 'password';
    spyOn(customerService, 'searchCustomer').and.returnValue(customer);
    expect(customerService.searchCustomer(customer).username).toEqual('testuser');
    expect(customerService.searchCustomer(customer).password).toEqual('password');
    component.ngOnInit();
    component.login();
    expect(authService.loggedIn).toEqual(true);    
  });

  it('auth service isAuthenticated()',() => {
    let customer = new Customer();
    customer.username = 'testuser';
    customer.password = 'password';
    spyOn(authService, 'isAuthenticated').and.returnValue(Promise.resolve(true));   
    let result = false;
    authService.isAuthenticated().then((value:boolean)=> {      
      expect(value).toEqual(true);
      result = value;
    });    
    
    
  });

 });
