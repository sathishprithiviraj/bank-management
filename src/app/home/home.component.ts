import { Component, OnInit } from '@angular/core';
import { LoginProfileService } from '../login/login-profile.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userName: string;
  constructor(private loginService: LoginProfileService) { }

  ngOnInit(): void {
    this.userName = this.loginService.getUserName();
  }


}
