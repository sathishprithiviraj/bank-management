import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanViewComponent } from './loan-view.component';
import { LoginProfileService } from '../../login/login-profile.service';
import { LoanService } from '../../loan/loan.service';
import { Loan } from '../../loan/loan.model';

describe('LoanViewComponent unit tests', () => {
  let component: LoanViewComponent;
  let fixture: ComponentFixture<LoanViewComponent>;
  let loginService: LoginProfileService;
  let loanService: LoanService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanViewComponent);
    component = fixture.componentInstance;
    loginService=fixture.debugElement.injector.get(LoginProfileService);
    loanService=fixture.debugElement.injector.get(LoanService);
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check loan user', () => {
    loginService.setUserName('TestUser');
    const component = new LoanViewComponent(loginService, loanService);
    component.userName = loginService.getUserName();
    expect(component.userName).toEqual('TestUser');
   });

   it('check loan view', () => {
     loginService.setUserName('TestUser');
     let loan = new Loan();
     loan.loanType='Education';
     loan.loanAmount=10000;
     loan.loanApplyDate='2021/1/30';
     loan.loanIssueDate='2021/2/30';
     
     loanService.createLoan(loan);    
    component.ngOnInit();
    expect(component.loansView[0].loanType).toEqual('Education');
    expect(component.loansView[0].loanAmount).toEqual(10000);
    expect(component.loansView[0].loanApplyDate).toEqual('2021/1/30');
    expect(component.loansView[0].loanIssueDate).toEqual('2021/2/30');
   });

});


