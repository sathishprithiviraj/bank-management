import { Component, OnInit } from '@angular/core';
import { LoanService } from '../../loan/loan.service';
import { Loan } from '../../loan/loan.model';
import { LoginProfileService } from '../../login/login-profile.service';

@Component({
  selector: 'app-loan-view',
  templateUrl: './loan-view.component.html',
  styleUrls: ['./loan-view.component.css']
})
export class LoanViewComponent implements OnInit {

  loansView:Loan[];
  userName:string;
  constructor(private loginService: LoginProfileService, private loanService: LoanService) { }

  ngOnInit(): void {
    this.loansView = this.loanService.getLoans();
    this.userName=this.loginService.getUserName();
    for (let l of this.loansView) {
      console.log(l.loanUser);
      console.log(this.loginService.getUserName());
    }
  }

}
